import { Todo, TodoStatus } from "@/typings/todo";
import { Ref, ref } from "vue";

type Result = [true, Ref<Todo[]>] | [false, string]

export default () => {
  const todos: Ref<Todo[]> = ref([])

  function fetchTodos() {
    return todos
  }

  function addTodo(title: string): Result {
    const maxId = todos.value
      .map(t => parseInt(t.id))
      .reduce((pre, cur) => (pre > cur ? pre : cur), 0)
    const todo: Todo = {
      id: (maxId + 1).toString(),
      title: title,
      status: TodoStatus.NOT_YET,
      createdAt: new Date()
    }
    todos.value.push(todo)
    return [true, fetchTodos()]
  }

  function changeStatus(id: string, status?: TodoStatus): Result {
    const todo = todos.value.find(t => t.id == id)
    if (todo == null) {
      return [
        false,
        "todo is not found."
      ]
    }
    if (status == null) {
      if (todo.status == TodoStatus.NOT_YET) {
        status = TodoStatus.DONE
      } else {
        status = TodoStatus.NOT_YET
      }
    }
    todo.status = status
    return [true, fetchTodos()]
  }

  return { fetchTodos, addTodo, changeStatus }
}
