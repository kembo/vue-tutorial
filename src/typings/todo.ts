export const TodoStatus = {
    NOT_YET: 0,
    DONE: 1
} as const

export type TodoStatus = typeof TodoStatus[keyof typeof TodoStatus]

export interface Todo {
    id: string;
    title: string;
    status: TodoStatus;
    createdAt: Date;
}
